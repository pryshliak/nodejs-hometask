const { user } = require('../models/user');
const { body, check, validationResult } = require('express-validator');
const { compareKeys } = require('./helpers/validator.helper');
const UserService = require('../services/userService');

//[TASK] Зайві поля не повинні пройти в БД

const userValidator = [
    check('firstName').exists().withMessage('First name is missing'),
    check('lastName').exists().withMessage('Last Name is missing'),
    check('email').exists().withMessage('User email is missing')
        .custom( val => val.endsWith('@gmail.com') )
        .withMessage("Email isn't gmail.com")
        // Check if there already is a user with this email address
        .custom(val => {
            const users = UserService.getUsers();
            const isUsersEmails = users.filter( el => el.email === val)

            return isUsersEmails.length > 0 ? false : true ;
        })
        .withMessage('There is already a user with this email'),
    check('phoneNumber').exists().withMessage('User phone is missing')
        .isLength({min:13, max:13}).withMessage('Phone number format is +380XXXXXXXXX')
        .custom( val=> val.startsWith('+380'))
        .withMessage('Phone number format is +380XXXXXXXXX')
        // Check if there already is a user with such phone number
        .custom(val=>{
            const users = UserService.getUsers();
            const isUsersPhone = users.filter( el => el.phoneNumber === val)

            return isUsersPhone.length > 0 ? false : true ;
        })
        .withMessage('There is already a user with this phone number'),
    check('password').exists().withMessage('User password is missing')
        .isLength({min:3})
        .withMessage('Use at least 3 characters for the password'),
    body().custom(el=>{
        return compareKeys(user, el) ? true : false;
        }).withMessage('Something went wrong')
]

const createUserValid = [
    ...userValidator,
    // TODO: Implement validatior for user entity during creation
    (req,res,next)=>{
        const errorFormatter = ({ location, msg, param, value, nestedErrors }) => {
            return `${msg}`;
        }
        const errors = validationResult(req).formatWith(errorFormatter);
        if(!errors.isEmpty()){
            return res.status(400).json({
                error: true,
                message: errors.array(),
            });
        }
        next()
    }
]; 

const updateUserValid = [ 
    ...userValidator,

    (req, res, next) => {
    // TODO: Implement validatior for user entity during update
    const errorFormatter = ({ location, msg, param, value, nestedErrors }) => {
        return `${msg}`;
    }
    const errors = validationResult(req).formatWith(errorFormatter);
    if(!errors.isEmpty()){
        return res.status(400).json({
            error: true,
            message: errors.array(),
        });
    }
    
    next();
    }
]

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;
