/**
 * Compare keys from objects and retur true of false.
 *
 * @param {obj} x Obj with fighter/user models from models folder .
 * @param {obj} x Obj with requst body.
 * @return {boolean} x True or false.
 */

exports.compareKeys = (firstObj, secondObj) => {
  const copyOffirstObj = { ...firstObj };
  const aKeys = Object.keys(copyOffirstObj).sort();
  const bKeys = Object.keys(secondObj).sort();

  if (aKeys.indexOf('id') !== -1) {
    // Remove the identifier from the object because we do not have the identifier pairs in req.body
    aKeys.splice(aKeys.indexOf('id'), 1);
  }

  return JSON.stringify(aKeys) === JSON.stringify(bKeys);
};
