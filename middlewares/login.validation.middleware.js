const { body, check, validationResult } = require('express-validator');
const { compareKeys } = require('./helpers/validator.helper');

//[TASK] Зайві поля не повинні пройти в БД

exports.validateUserLogin = [
  check('email').exists().withMessage('Email is required'),
  check('password').exists().withMessage('Password is required'),
  body()
    .custom((val) => {
      const allowedFields = { email: 'string', password: 'string' };
      return compareKeys(allowedFields, val) ? true : false;
    }).withMessage('Something went wrong..'),

  (req, res, next) => {
    const errorFormatter = ({ location, msg, param, value, nestedErrors }) => {
      return `${msg}`;
    };
    const errors = validationResult(req).formatWith(errorFormatter);
    if (!errors.isEmpty()) {
      res.status(400).json({
        error: true,
        message: errors.array(),
      });
    }
    next();
  },
];
