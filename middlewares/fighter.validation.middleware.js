const { fighter } = require('../models/fighter');
const { body, check, validationResult } = require('express-validator');
const { compareKeys } = require('./helpers/validator.helper');
const FighterService = require('../services/fighterService')

//[TASK] Зайві поля не повинні пройти в БД

const fighterValidator = [
    check('name').exists().withMessage('Fighter name is required'),
    check('health').exists().withMessage('Health is required')
        .isNumeric().withMessage('Health field should be number')
        .isInt({gt: 1, lt:101}).withMessage('Health should be more then 1 and less or equal 100'),
    check('power').exists().withMessage('Power is required')
        .isNumeric().withMessage('Power field should be number')
        .isInt({gt: 1, lt:100}).withMessage('Power should be more then 1 and less then 100'),
    check('defense').exists().withMessage('Defense is required')
        .isNumeric().withMessage('Defense field should be number')
        .isInt({gt: 1, lt:11}).withMessage('Defense should be more then 1 and less or equal 10'),
    body().custom(val=>{
        return compareKeys(fighter, val) ? true:false
    }).withMessage('Something went wrong..'),
    // Check for fighters with same name
    check('name').custom(val=>{
        const fighters = FighterService.getFighter();
        const fightersNames = fighters.filter( el => el.name === val)

        return fightersNames.length > 0 ? false : true
    }).withMessage('There is a fighter with that name')
]

const createFighterValid = [
    ...fighterValidator,
    // TODO: Implement validatior for fighter entity during creation
    (req,res,next)=>{
        const errorFormatter = ({ location, msg, param, value, nestedErrors }) => {
            return `${msg}`;
        }
        const errors = validationResult(req).formatWith(errorFormatter);
        if(!errors.isEmpty()){
            return res.status(400).json({
                error: true,
                message: errors.array(),
            });
        }
        next()
    }
]; 

const updateFighterValid = [
    ...fighterValidator,
    // TODO: Implement validatior for fighter entity during creation
    (req,res,next)=>{
        const errorFormatter = ({ location, msg, param, value, nestedErrors }) => {
            return `${msg}`;
        }
        const errors = validationResult(req).formatWith(errorFormatter);
        if(!errors.isEmpty()){
            return res.status(400).json({
                error: true,
                message: errors.array(),
            });
        }
        next()
    }
]

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;