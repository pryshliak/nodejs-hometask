import React from "react";
//Create health bar for fighters
export default function CreateHealthIndicators({ fighter, position }) {
  const { name } = fighter;
  const id = `${position}-fighter-indicator`;
  return (
    <div className="arena___fighter-indicator">
      <span className="arena___fighter-name">{name}</span>
      <div className="arena___health-indicator">
        <div className="arena___health-bar" id={id}></div>
      </div>
    </div>
  );
}
