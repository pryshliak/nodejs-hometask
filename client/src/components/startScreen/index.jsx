import * as React from "react";
import SignInUpPage from "../signInUpPage";
import { isSignedIn } from "../../services/authService";
import Fight from "../fight";
import SignOut from "../signOut";
import FightArena from "../arena";

class StartScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isSignedIn: false,
      isInArena: false,
      fighter1: null,
      fighter2: null,
    };
  }

  componentDidMount() {
    this.setIsLoggedIn(isSignedIn());
  }

  completeFight = () => {
    this.setState({
      isInArena: false,
    });
  };

  setIsLoggedIn = (isSignedIn) => {
    this.setState({ isSignedIn });
  };

  startFight = (fighter1, fighter2) => {
    this.setState({
      isInArena: true,
      fighter1,
      fighter2,
    });
  };

  render() {
    const { isSignedIn, isInArena, fighter1, fighter2 } = this.state;

    if (!isSignedIn) {
      return <SignInUpPage setIsLoggedIn={this.setIsLoggedIn} />;
    }

    if (isInArena) {
      return (
        <FightArena
          onCompleteFight={this.completeFight}
          fighter1={fighter1}
          fighter2={fighter2}
        />
      );
    }

    return (
      <>
        <Fight onStartFight={this.startFight} />
        <SignOut
          isSignedIn={isSignedIn}
          onSignOut={() => this.setIsLoggedIn(false)}
        />
      </>
    );
  }
}

export default StartScreen;
