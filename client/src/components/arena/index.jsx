import React, { useState, useEffect } from "react";

import { createFight } from "../../services/domainRequest/fightRequest";
import { getObjectFromLocalStorage } from "../../services/localStorageHelper";
import Modal from "../modal";
import CreateHealthIndicators from "../healthindicator";
import FighterElement from "../fighterElement";
import { fight } from "./helper/fightActions";

import "./arena.css";
// The main function for the battle
function FightArena(props) {
  const [winner, setWinner] = useState(false);

  useEffect(() => {
    const { fighter1, fighter2 } = props;

    fight(fighter1, fighter2).then((fighter) => {
      const { winner, loser } = fighter;
      setWinner(winner);
    });
  });

  const { fighter1, fighter2, onCompleteFight } = props;

  const arena = winner ? (
    <Modal fighter={winner} onClose={onCompleteFight} />
  ) : (
    <div className="arena___root">
      <div>
        <div className="arena___fight-status">
          <CreateHealthIndicators fighter={fighter1} position="left" />
          <div className="arena___versus-sign">
            <img src="" alt="vs-sign" />
          </div>
          <CreateHealthIndicators fighter={fighter2} position="right" />
        </div>
        <div className="arena___battlefield">
          <FighterElement fighter={fighter1} position="left" />
          <FighterElement fighter={fighter2} position="right" />
        </div>
      </div>
    </div>
  );

  return <div className="arena___root">{arena}</div>;
}

export default FightArena;
