import { controls } from "../../../constants/fightControls";

export async function fight(firstFighter, secondFighter) {
  console.log("fight", firstFighter, secondFighter);
  return new Promise((resolve) => {
    //[TASK] resolve the promise with the winner when fight is over
    const selectedFirstFighter = initArenaFighter(firstFighter);
    const selectedSecondFighter = initArenaFighter(secondFighter);
    const pressedKeys = new Map();

    // Start DOMtimer and set timout to 90 sec (Max fight time)
    globalGameTimer(90);
    setTimeout(() => {
      const firstFighterHealth = Math.round(
        (selectedFirstFighter.currentHealth * 100) / selectedFirstFighter.health
      );
      const secondFighterHealth = Math.round(
        (selectedSecondFighter.currentHealth * 100) /
        selectedSecondFighter.health
      );
      return secondFighterHealth > firstFighterHealth
        ? resolve({ winner: secondFighter, loser: firstFighter })
        : resolve({ winner: firstFighter, loser: secondFighter });
    }, 89000);

    //Listen to pressed keys
    document.addEventListener("keydown", (e) => {
      pressedKeys.set(e.code, true);

      fightActions(selectedFirstFighter, selectedSecondFighter, pressedKeys);

      if (selectedSecondFighter.currentHealth <= 0) {
        resolve({ winner: firstFighter, loser: secondFighter });
      }
      if (selectedFirstFighter.currentHealth <= 0) {
        resolve({ winner: secondFighter, loser: firstFighter });
      }
    });

    document.addEventListener("keyup", (e) => {
      pressedKeys.delete(e.code);
    });
  });
}
/**
 * Returns fighter data object.
 *
 * @param {object} x The fighter data object.
 * @return {object} x With information about the fighter and the critical time of impact
 */
function initArenaFighter(fighter) {
  return {
    ...fighter,
    currentHealth: fighter.health,
    criticalHitCooldown: new Date(),
    setCriticalHitTimer() {
      this.criticalHitCooldown = new Date();
    },
  };
}
/**
 * The main combat function, which checks which keys are pressed and applies the required functions.
 *
 * @param {object} x The firstFighter data object.
 * @param {object} x The secondFighter data object.
 * @param {object} x The pressed keys.
 */
function fightActions(firstFighter, secondFighter, keyMap) {
  const firstFighterHealthBar = document.getElementById(
    "left-fighter-indicator"
  );
  const secondFighterHealthBar = document.getElementById(
    "right-fighter-indicator"
  );

  switch (true) {
    case keyMap.has(controls.PlayerOneAttack):
      {
        attackAction(
          firstFighter,
          secondFighter,
          secondFighterHealthBar,
          keyMap
        );
      }
      break;
    case keyMap.has(controls.PlayerTwoAttack):
      {
        attackAction(
          secondFighter,
          firstFighter,
          firstFighterHealthBar,
          keyMap
        );
      }
      break;
    case controls.PlayerOneCriticalHitCombination.every((code) =>
      keyMap.has(code)
    ):
      {
        criticalAttackAction(
          firstFighter,
          secondFighter,
          secondFighterHealthBar
        );
      }
      break;
    case controls.PlayerTwoCriticalHitCombination.every((code) =>
      keyMap.has(code)
    ):
      {
        criticalAttackAction(
          secondFighter,
          firstFighter,
          firstFighterHealthBar
        );
      }
      break;
  }
}

function getDamage(attacker, defender) {
  //[TASK] return damage
  const damage = getHitPower(attacker) - getBlockPower(defender);
  //return the damage if it is greater than 0 otherwise 0
  return damage > 0 ? damage : 0;
}

function getHitPower(fighter) {
  //[TASK] return hit power

  //get random number 1 or 2
  const criticalHitChance = Math.random() + 1;
  const { power } = fighter;

  return power * criticalHitChance;
}

function getBlockPower(fighter) {
  //[TASK] return block power

  //get random number 1 or 2
  const dodgeChance = Math.random() + 1;
  const { defense } = fighter;

  return defense * dodgeChance;
}

function isCriticalHit(attacker) {
  // check critical hit time
  const cooldownSeconds =
    (new Date().getTime() - attacker.criticalHitCooldown.getTime()) / 1000;
  return cooldownSeconds < 10;
}

function isAttackBlocked(keyMap) {
  // check that the user has pressed the key to block the kick
  return ( keyMap.has(controls.PlayerOneBlock) || keyMap.has(controls.PlayerTwoBlock) );
}
//function that updates health indicator
function updateHealthIndicator(defender, indicator) {
  try{
    const { health, currentHealth } = defender;

    const indicatorWidth = Math.max(0, (currentHealth * 100) / health);
    indicator.style.width = `${indicatorWidth}%`;
    indicator.innerText = `${Math.round(indicatorWidth)}%`;
  }catch(e){
    console.error(e)
  }
}
//function that handles critical impact
function criticalAttackAction(attacker, defender, healthIndicator) {
  try{
    if (isCriticalHit(attacker)) {return };

    defender.currentHealth -= attacker.power * 2;
    updateHealthIndicator(defender, healthIndicator);
    //update critical hit timer
    attacker.setCriticalHitTimer();
  }catch(e){
    console.error(e)
  }
}
//function that handles simple impact
function attackAction(attacker, defender, healthIndicator, keyMap) {
  try{
    if (isAttackBlocked(keyMap)) {
      return;
    }
  
    defender.currentHealth -= getDamage(attacker, defender);
    updateHealthIndicator(defender, healthIndicator);
  }catch(e){
    console.error(e)
  }
}

function globalGameTimer(fightSeconds) {
  const timerDomElement = document.querySelector(".arena___versus-sign");
  let counter = 0;

  timerDomElement.innerText = fightSeconds - counter;

  function timeInt() {
    let timeLeft = fightSeconds - counter;

    counter++;
    timerDomElement.innerText = timeLeft;

    if (timeLeft === 0) {
      clearInterval(fightInterval);
    }
  }

  const fightInterval = setInterval(timeInt, 1000);
}
