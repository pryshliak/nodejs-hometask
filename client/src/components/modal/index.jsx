import React from "react";

import "./modal.css";
// Create a modal window for fight winner
export default function createModal({ fighter, onClose = () => {} }) {
  return (
    <div className="modal-layer">
      <div className="modal-root">
        <div className="modal-header">
          <span>{fighter.name} won ! </span>
          <button className="close-btn" onClick={onClose}>
            X
          </button>
        </div>
        <div className="modal-body">
          <img src={fighter.img} alt={fighter.name} />
        </div>
      </div>
    </div>
  );
}
