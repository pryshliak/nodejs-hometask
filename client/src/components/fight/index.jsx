import React from "react";

import { getFighters } from "../../services/domainRequest/fightersRequest";
import NewFighter from "../newFighter";
import Fighter from "../fighter";
import { Button } from "@material-ui/core";

import "./fight.css";

class Fight extends React.Component {
  state = {
    fighters: [],
    fighter1: null,
    fighter2: null,
  };

  async componentDidMount() {
    const fighters = await getFighters();
    if (fighters && !fighters.error) {
      this.setState({ fighters });
    }
  }

  onFightStart = () => {
    const { fighter1, fighter2 } = this.state;
    if (fighter1 && fighter2) {
      this.props.onStartFight(fighter1, fighter2);
    } else {
      alert("Please, choose fighters");
    }
  };

  onCreate = (fighter) => {
    this.setState({ fighters: [...this.state.fighters, fighter] });
  };

  onfighter1Select = (fighter1) => {
    this.setState({ fighter1 });
  };

  onfighter2Select = (fighter2) => {
    this.setState({ fighter2 });
  };

  getfighter1List = () => {
    const { fighter2, fighters } = this.state;
    if (!fighter2) {
      return fighters;
    }

    return fighters.filter((it) => it.id !== fighter2.id);
  };

  getfighter2List = () => {
    const { fighter1, fighters } = this.state;
    if (!fighter1) {
      return fighters;
    }

    return fighters.filter((it) => it.id !== fighter1.id);
  };

  render() {
    const { fighter1, fighter2 } = this.state;
    const fighter1Img = "https://media.giphy.com/media/kdHa4JvihB2gM/giphy.gif";
    const fighter2Img =
      "https://i.pinimg.com/originals/46/4b/36/464b36a7aecd988e3c51e56a823dbedc.gif";
    return (
      <div id="wrapper">
        <NewFighter onCreated={this.onCreate} />
        <div id="figh-wrapper">
          <Fighter
            selectedFighter={fighter1}
            onFighterSelect={this.onfighter1Select}
            fightersList={this.getfighter1List() || []}
            fighterImg={fighter1Img}
          />
          <div className="btn-wrapper">
            <Button
              onClick={this.onFightStart}
              variant="contained"
              color="primary"
            >
              Start Fight
            </Button>
          </div>
          <Fighter
            selectedFighter={fighter2}
            onFighterSelect={this.onfighter2Select}
            fightersList={this.getfighter2List() || []}
            fighterImg={fighter2Img}
          />
        </div>
      </div>
    );
  }
}

export default Fight;
