import React from "react";
//Create fighter on arena
export default function FighterElement({ fighter, position }) {
  const posClassName = `arena___${position}-fighter`;
  const fighterClass = `arena___fighter ${posClassName}`;

  return (
    <div className={fighterClass}>
      <img
        className="fighter-preview___img"
        src={fighter.img}
        alt={fighter.name}
      />
    </div>
  );
}
