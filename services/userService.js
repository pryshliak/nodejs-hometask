const { UserRepository } = require('../repositories/userRepository');

class UserService {
  // TODO: Implement methods to work with user
  create(user) {
    try {
      return UserRepository.create(user);
    } catch (e) {
      throw Error('Error while create Users');
    }
  }

  getUsers() {
    try {
      const users = UserRepository.getAll();

      return !users ? null : users;
    } catch (e) {
      throw Error('Error while getUsers ');
    }
  }

  update(id, data) {
    try {
      const updatedUser = UserRepository.update(id, data);

      return !updatedUser ? null : updatedUser;
    } catch (e) {
      throw Error('Error while updating Users');
    }
  }

  remove(id) {
    try {
      const removedUser = UserRepository.delete(id);

      return !removedUser ? null : removedUser;
    } catch (e) {
      throw Error('Error while removing Users');
    }
  }

  search(search) {
    const item = UserRepository.getOne(search);
    if (!item) {
      return null;
    }
    return item;
  }
}

module.exports = new UserService();