const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
  // TODO: Implement methods to work with fighters
  create(fighter) {
    try {
      return FighterRepository.create(fighter);
    } catch (e) {
      throw Error('Error while create Fighter');
    }
  }

  getFighter() {
    try {
      const fighters = FighterRepository.getAll();

      return !fighters ? null : fighters;
    } catch (e) {
      throw Error('Error while get Fighter');
    }
  }

  update(id, data) {
    try {
      const updatedFighter = FighterRepository.update(id, data);

      return !updatedFighter ? null : updatedFighter;
    } catch (e) {
      throw Error('Error while updating Fighter ');
    }
  }

  remove(id) {
    try {
      const removedFighter = FighterRepository.delete(id);

      return !removedFighter ? null : removedFighter;
    } catch (e) {
      throw Error('Error while removing Fighter ');
    }
  }

  search(search) {
    try {
      const item = FighterRepository.getOne(search);

      return !item ? null : item;
    } catch (e) {
      throw Error('Error while searching Fighter ');
    }
  }
}

module.exports = new FighterService();
