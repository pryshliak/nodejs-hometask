const { FightRepository } = require('../repositories/fightRepository');

class FightersService {
  // OPTIONAL TODO: Implement methods to work with fights
  create(fight) {
    try {
      return FightRepository.create(fight);
    } catch (e) {
      throw Error('Error while create Fight');
    }
  }

  getFight() {
    try {
      const fighters = FightRepository.getAll();

      return !fighters ? null : fighters;
    } catch (e) {
      throw Error('Error while get Fights');
    }
  }

  update(id, data) {
    try {
      const updatedFight = FightRepository.update(id, data);

      return !updatedFight ? null : updatedFight;
    } catch (e) {
      throw Error('Error while updating Fight');
    }
  }

  remove(id) {
    try {
      const removedFight = FightRepository.delete(id);

      return !removedFight ? null : removedFight;
    } catch (e) {
      throw Error('Error while removing Fight');
    }
  }

  search(search) {
    try {
      const item = FightRepository.getOne(search);

      return !item ? null : item;
    } catch (e) {
      throw Error('Error while searching Fight');
    }
  }
}

module.exports = new FightersService();
