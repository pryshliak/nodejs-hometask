const { Router } = require('express');
const UserService = require('../services/userService');
const {
  createUserValid,
  updateUserValid,
} = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

// TODO: Implement route controllers for user

//  GET /api/users
router.get('/', (req, res) => {
  const user = UserService.getUsers();

  if (user) {
    res.json(user);
  } else {
    res.status(400).json({
      error: true,
      message: 'There are no users with these parameters',
    });
  }
});

//  GET /api/users/:id
router.get('/:id', (req, res) => {
  const id = req.params.id;
  const searchUser = UserService.search({ id });

  if (searchUser) {
    res.json(searchUser);
  } else {
    res.status(404).json({
      error: true,
      message: 'There are no users with such id',
    });
  }
});

//  POST /api/users
router.post('/', createUserValid, (req, res) => {
  const user = req.body;
  const createUser = UserService.create(user);

  if (createUser) {
    res.json(createUser);
  } else {
    res.status(400).json({
      error: true,
      message: 'We cannot create such a user',
    });
  }
});

//  PUT /api/users/:id
router.put('/:id', updateUserValid, (req, res) => {
  const id = req.params.id;
  const userInfo = req.body;
  const updateUser = UserService.update(id, userInfo);

  if (updateUser) {
    res.json(updateUser);
  } else {
    res.status(404).json({
      error: true,
      message: 'There are no users with such id',
    });
  }
});

//  DELETE /api/users/:id
router.delete('/:id', (req, res) => {
  const id = req.params.id;
  const deleteUser = UserService.remove(id);

  if (deleteUser) {
    res.json(deleteUser);
  } else {
    res.status(404).json({
      error: true,
      message: 'There are no users with such id',
    });
  }
});

module.exports = router;
