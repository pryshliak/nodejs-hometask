const { Router } = require('express');
const AuthService = require('../services/authService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { validateUserLogin } = require('../middlewares/login.validation.middleware');

const router = Router();

router.post('/login', validateUserLogin, (req, res, next) => {
    try {
      // TODO: Implement login action (get the user if it exist with entered credentials)
      const { password, email } = req.body;
      const user = AuthService.login({ email });

      if (!user) {
        res.status(400).json({
          error: true,
          message: 'There is no account with such Email',
        });
      }
      if (password === user.password) {
        res.json(user);
      } else {
        res.status(400).json({
          error: true,
          message: 'There is no account with such Password',
        });
      }
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

module.exports = router;
