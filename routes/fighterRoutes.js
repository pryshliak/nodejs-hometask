const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

// TODO: Implement route controllers for fighter

//  GET /api/fighters
router.get('/', (req, res) => {
  const fighter = FighterService.getFighter();

  if (fighter) {
    res.json(fighter);
  } else {
    res.status(404).json({
      error: true,
      message: 'There are no fighters',
    });
  }
});

//  GET /api/fighters/:id
router.get('/:id', (req, res) => {
  const id = req.params.id;
  const searchFighter = FighterService.search({ id });

  if (searchFighter) {
    res.status(200).json(searchFighter);
  } else {
    res.status(404).json({
      error: true,
      message: 'There are no fighters with such id',
    });
  }
});

//  POST /api/fighters
router.post('/', createFighterValid, (req, res) => {
  const fighter = req.body;
  const createFighter = FighterService.create(fighter);

  if (createFighter) {
    res.status(200).json(createFighter);
  } else {
    res.status(400).json({
      error: true,
      message: "We can't create fighter",
    });
  }
});

//  PUT /api/fighters/:id
router.put('/:id', updateFighterValid, (req, res) => {
  const id = req.params.id;
  const userInfo = req.body;
  const updateFighter = FighterService.update(id, userInfo);

  if (updateFighter) {
    res.status(200).json(updateFighter);
  } else {
    res.status(404).json({
      error: true,
      message: 'There are no fighter with such id',
    });
  }
});

//  DELETE /api/fighters/:id
router.delete('/:id', (req, res) => {
  const id = req.params.id;
  const deleteFighter = FighterService.remove(id);

  if (deleteFighter) {
    res.status(200).json(deleteFighter);
  } else {
    res.status(404).json({
      error: true,
      message: 'There are no fighter with such id',
    });
  }
});

module.exports = router;
