const { Router } = require('express');
const FightService = require('../services/fightService');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

// OPTIONAL TODO: Implement route controller for fights

//  GET /api/fighters
router.get('/', (req, res) => {
  const fight = FightService.getFight();

  if (fight) {
    res.json(fight);
  } else {
    res.status(404).json({
      error: true,
      message: 'Ups...',
    });
  }
});

//  GET /api/fighters/:id
router.get('/:id', (req, res) => {
  const id = req.params.id;
  const searchFight = FightService.search({ id });

  if (searchFight) {
    res.status(200).json(searchFight);
  } else {
    res.status(404).json({
      error: true,
      message: 'Ups...',
    });
  }
});

//  POST /api/fighters
router.post('/', (req, res) => {
  const fight = req.body;
  const createFight = FightService.create(fight);

  if (createFight) {
    res.status(200).json(createFight);
  } else {
    res.status(400).json({
      error: true,
      message: 'Ups...',
    });
  }
});

//  PUT /api/fighters/:id
router.put('/:id', (req, res) => {
  const id = req.params.id;
  const fightInfo = req.body;
  const updateFight = FightService.update(id, fightInfo);

  if (updateFight) {
    res.status(200).json(updateFight);
  } else {
    res.status(404).json({
      error: true,
      message: 'Ups....',
    });
  }
});

//  DELETE /api/fighters/:id
router.delete('/:id', (req, res) => {
  const id = req.params.id;
  const deleteFight = FightService.remove(id);

  if (deleteFight) {
    res.status(200).json(deleteFight);
  } else {
    res.status(404).json({
      error: true,
      message: 'There are no fighter with such id',
    });
  }
});

module.exports = router;
